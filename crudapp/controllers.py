#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import HttpResponse
from django.conf import settings

#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from login.oauth import *

from rest_framework.exceptions import PermissionDenied

class Crudapp(viewsets.ModelViewSet):
    serializer_class = CrudappSerializer
    queryset = Book.objects.all()

    def create(self, request) :
        form_serializer = CrudappSerializer(data=request.data)
        if is_authenticated(request) :
            if form_serializer.is_valid():
                form_serializer.save()
            return Response(form_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({"Message": "Invalid token"}, status=status.HTTP_401_UNAUTHORIZED)

    def list(self, request) :
        if is_authenticated(request) :
            qry = Book.objects.all()
            serializer = CrudappSerializer(qry, many=True)
            return Response({'result' : serializer.data})
        else:
            return Response({'Message': 'Invalid token'}, status=status.HTTP_401_UNAUTHORIZED)

    def retrieve(self, request, pk) :
        if is_authenticated(request) :
            instance = Book.objects.get(id=pk)
            serializer = CrudappSerializer(instance)
            return Response(serializer.data)
        return Response({"Message": "Invalid token"}, status=status.HTTP_401_UNAUTHORIZED)

    def update(self, request, *args, **kwargs):
        if is_authenticated(request):
            partial = kwargs.pop("partial", False)
            instance = self.get_object()
            instance.judul = request.data.get("judul")
            instance.pengarang = request.data.get("pengarang")
            instance.penerbit = request.data.get("penerbit")
            instance.tahun_terbit = request.data.get("tahun_terbit")
            instance.save()
            serializer = self.get_serializer(instance)
            if getattr(instance, '_prefetched_objects_cache', None):
                instance._prefetched_objects_cache = {}
                return Response(serializer.data)
            else:
                return Response(serializer.data)

        else:
            return Response({"Message": "Invalid token"}, status=status.HTTP_401_UNAUTHORIZED)

    def destroy(self, request, pk) :
        if is_authenticated(request):
            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response({"Message": "Invalid token"}, status=status.HTTP_401_UNAUTHORIZED)
