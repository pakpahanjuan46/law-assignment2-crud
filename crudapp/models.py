from django.db import models
from django.contrib.auth.models import *

class Book(models.Model):
    judul = models.CharField(max_length=128)
    pengarang = models.CharField(max_length=128)
    penerbit = models.CharField(max_length=128)
    tahun_terbit = models.PositiveIntegerField()
    
    class Meta:
        db_table = 'Book'
