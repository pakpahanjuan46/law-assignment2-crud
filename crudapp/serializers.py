#rest framework
from rest_framework import *
from rest_framework.serializers import *
from rest_framework.authtoken.models import *

#django
from django.contrib.auth.models import *

from .models import *
class CrudappSerializer(ModelSerializer):
	class Meta:
		model = Book
		fields = "__all__"
