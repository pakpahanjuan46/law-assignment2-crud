from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from rest_framework.routers import DefaultRouter

from . import controllers

router = DefaultRouter()
router.register(r'', controllers.Crudapp, basename='')


urlpatterns = [
    path('', include(router.urls)),
]