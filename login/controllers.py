#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *

#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from .oauth import *

from rest_framework.exceptions import PermissionDenied

class Login(viewsets.ModelViewSet):
	serializer_class = LoginSerializer
	queryset = {}

	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid()
		username = serializer.data['username']
		password = serializer.data['password']
		token = get_token(username, password)

		if token == None :
			return Response({'result': 'invalid token'}, status=status.HTTP_401_UNAUTHORIZED)

		request.session['authorization'] = 'Bearer ' + token
		return Response({'success': True}, status=status.HTTP_202_ACCEPTED)



		
