#rest framework
from rest_framework import *
from rest_framework.serializers import *
from rest_framework.authtoken.models import *

#django
from django.contrib.auth.models import *

from .models import *
class LoginSerializer(Serializer):
    username = CharField(max_length=128)
    password = CharField(style={'input_type': 'password'})